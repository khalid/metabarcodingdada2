MenuGauche = sidebarMenu(id="sidebarmenu",

	menuItem("Global parameters", tabName="global_params", icon=icon("pencil", lib="font-awesome"), newtab=FALSE),

	menuItem("Trimming", tabName="trimming", icon=icon("pencil", lib="font-awesome"), newtab=FALSE),

	menuItem("Dada2", tabName="dada2", icon=icon("pencil", lib="font-awesome"), newtab=FALSE),

	menuItem("Draw workflow graph", tabName="RULEGRAPH", icon=icon("gear", lib="font-awesome"), newtab=FALSE),

	tags$br(),

	downloadButton("DownloadParams", "Download config file", class="btn btn-light", style="color:black;margin: 6px 5px 6px 15px;"),

	tags$br(),

	tags$br(),

	numericInput("cores", label = "Threads available", min = 1, max = 24, step = 1, width =  "auto", value = 16),
selectInput("force_from", label = "Start again from a step : ", selected = "none", choices = list('No'='none','Trimming'='trimming','Dada2'='dada2')),	tags$br(),
	actionButton("RunPipeline", "Run pipeline",  icon("play"), class="btn btn-info"),

	actionButton("StopPipeline", "Stop pipeline",  icon("stop"), class="btn btn-secondary"),
	tags$br(),
	tags$br(),
	menuItem("Running Workflow output", tabName="run_out", icon=icon("terminal", lib="font-awesome"), newtab=FALSE),
	menuItem("Final report", tabName="Report", icon=icon("file", lib="font-awesome"), newtab=FALSE),

	tags$br(),
	actionButton("close_session", "Close session",  icon("times"), class="btn btn-primary"),
	tags$br(),tags$br(),

	menuItem("Powered by mbb", href="http://mbb.univ-montp2.fr/MBB/index.php", newtab=TRUE, icon=icon("book", lib="font-awesome"), selected=NULL)

)


