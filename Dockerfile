FROM mbbteam/mbb_workflows_base:latest as alltools

RUN pip3 install cutadapt==2.3

RUN apt-get install -y pigz

RUN Rscript -e 'library("devtools");devtools::install_github("benjjneb/dada2", ref="v1.16");library("dada2")'

RUN Rscript -e 'library("devtools");devtools::install_github("joey711/phyloseq");library("phyloseq")'

RUN Rscript -e 'library("devtools");devtools::install_github("joey711/biomformat");library("biomformat")'

RUN cd /opt/biotools \
 && git clone https://github.com/marbl/Krona.git \
 && cd Krona/KronaTools \
 && ./install.pl --prefix /opt/biotools \
 && cd /opt/biotools \
 && rm -r Krona/ExcelTemplate

RUN cd /opt/biotools \
 && git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/mbb_mqc_plugin.git \
 && cd mbb_mqc_plugin \
 && python3 setup.py install

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY ./files/data/taxonomy /taxonomy
COPY files /workflow
COPY sagApp /sagApp

