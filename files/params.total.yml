pipeline: Metabarcoding
params:
  results_dir: /Results
  sample_dir: /Data
  SeOrPe: PE
  trimming: 'null'
  trimming__cutadapt_PE_output_dir: trimming/cutadapt_PE
  trimming__cutadapt_PE_command: cutadapt
  trimming__cutadapt_threads: 4
  trimming__cutadapt_qc_score: --quality-base=33
  trimming__cutadapt_qc_min: 20
  trimming__cutadapt_max_N: 0
  trimming__cutadapt_a: ''
  trimming__cutadapt_A: ''
  trimming__cutadapt_SE_output_dir: trimming/cutadapt_SE
  trimming__cutadapt_SE_command: cutadapt
  trimming__null_output_dir: trimming/
  trimming__null_command: ''
  dada2: dada2
  dada2__dada2_script: scripts/dada2.script.R
  dada2__dada2_output_dir: dada2/dada2
  dada2__dada2_command: null
  dada2__dada2_threads: 4
  dada2__dada2_no_filter_and_trim: false
  dada2__dada2_remove_chim: true
  dada2__dada2_taxonomypath: silva138 (16S)
  dada2__dada2_truncLen_forward: 0
  dada2__dada2_truncLen_reverse: 0
  dada2__dada2_maxEE_forward: -1
  dada2__dada2_maxEE_reverse: -1
steps:
- name: trimming
  title: Trimming
  tools:
  - cutadapt
  - 'null'
  default: 'null'
- name: dada2
  title: Dada2
  tools:
  - dada2
  default: dada2
params_info:
  results_dir:
    type: output_dir
  sample_dir:
    type: input_dir
  SeOrPe:
    type: radio
  trimming__cutadapt_threads:
    tool: cutadapt
    rule: trimming_cutadapt_SE
    type: numeric
    label: Number of threads to use
  trimming__cutadapt_qc_score:
    tool: cutadapt
    rule: trimming_cutadapt_SE
    type: radio
    label: Quality score encoding
  trimming__cutadapt_qc_min:
    tool: cutadapt
    rule: trimming_cutadapt_SE
    type: numeric
    label: trim low-quality 3'ends from reads before adapter removal
  trimming__cutadapt_max_N:
    tool: cutadapt
    rule: trimming_cutadapt_PE
    type: numeric
    label: Discard reads with more than X N bases. If X is an number between 0 and
      1, it is interpreted as a fraction of the read length
  trimming__cutadapt_a:
    tool: cutadapt
    rule: trimming_cutadapt_SE
    type: text
    label: Forward adapter sequence
  trimming__cutadapt_A:
    tool: cutadapt
    rule: trimming_cutadapt_PE
    type: text
    label: Reverse adapter sequence
  dada2__dada2_threads:
    tool: dada2
    rule: dada2_dada2
    type: numeric
    label: Number of threads to use
  dada2__dada2_no_filter_and_trim:
    tool: dada2
    rule: dada2_dada2
    type: checkbox
    label: Skip filter and trim step
  dada2__dada2_remove_chim:
    tool: dada2
    rule: dada2_dada2
    type: checkbox
    label: Remove chimeras
  dada2__dada2_taxonomypath:
    tool: dada2
    rule: dada2_dada2
    type: select
    label: Taxonomy source
  dada2__dada2_truncLen_forward:
    tool: dada2
    rule: dada2_dada2
    type: numeric
    label: 'truncLen forward : Truncate reads after truncLen bases. Reads shorter
      than this are discarded'
  dada2__dada2_truncLen_reverse:
    tool: dada2
    rule: dada2_dada2
    type: numeric
    label: 'truncLen reverse : Truncate reads after truncLen bases. Reads shorter
      than this are discarded'
  dada2__dada2_maxEE_forward:
    tool: dada2
    rule: dada2_dada2
    type: numeric
    label: 'maxEE forward : After truncation, reads with higher than maxEE ''expected
      errors'' will be discarded. Expected errors are calculated from the nominal
      definition of the quality score: EE = sum(10^(-Q/10)). Type -1 for Infinity
      (no filtering).'
  dada2__dada2_maxEE_reverse:
    tool: dada2
    rule: dada2_dada2
    type: numeric
    label: 'maxEE reverse : After truncation, reads with higher than maxEE ''expected
      errors'' will be discarded. Expected errors are calculated from the nominal
      definition of the quality score: EE = sum(10^(-Q/10)). Type -1 for Infinity
      (no filtering).'
prepare_report_scripts: []
prepare_report_outputs: {}
outputs:
  trimming__cutadapt:
    trimming__cutadapt_PE:
    - name: read_trimmed
      type: fq.gz
      file: '{sample}_trimmed_R1.fq.gz'
      description: Reads forward trimmed
    - name: read2_trimmed
      type: fq.gz
      file: '{sample}_trimmed_R2.fq.gz'
      description: Reads reverse trimmed
    trimming__cutadapt_SE:
    - name: read_trimmed
      type: fq.gz
      file: '{sample}_trimmed.fq.gz'
      description: Reads trimmed
  trimming__null:
    trimming__null: []
  dada2__dada2:
    dada2__dada2:
    - name: qualityForward
      type: png
      file: Quality_forward_mqc.png
      description: Quality graph of the forward reads of the first two samples
    - name: qualityReverse
      type: png
      file: Quality_reverse_mqc.png
      description: Quality graph of the reverse reads of the first two samples
    - name: filter_and_trim_out
      type: rds
      file: filter_and_trim_out.rds
      description: Post-filter and trimming report
    - name: filtFs
      type: fastq.gz
      file: filtered/{sample}_F_filt.fastq.gz
      description: List of filtered forward reads
    - name: filtRs
      type: fastq.gz
      file: filtered/{sample}_R_filt.fastq.gz
      description: List of filtered reverse reads
    - name: errorsForward
      type: png
      file: Errors_forward_mqc.png
      description: Error rate graph on forward reads
    - name: errorsReverse
      type: png
      file: Errors_reverse_mqc.png
      description: Error rate graph on reverse reads
    - name: errF_rds
      type: rds
      file: errF.rds
      description: Error prediction for forward reads
    - name: errR_rds
      type: rds
      file: errR.rds
      description: Error prediction for reverse reads
    - name: track
      type: tsv
      file: Track_reads_mqc.tsv
      description: Read count in the different steps
    - name: seqtab
      type: rds
      file: seqtab.rds
      description: Sequence table
    - name: seqtab_nochim_csv
      type: csv
      file: seqtab_nochim.csv
      description: Sequence table after removal of chimeras
    - name: seqtab_nochim_rds
      type: rds
      file: seqtab_nochim.rds
      description: Sequence table after removal of chimeras
    - name: uniques_nochim
      type: fasta
      file: uniques_nochim.fasta
      description: Unique sequences after removal of chimeras
    - name: taxa_bootstrap
      type: tsv
      file: Taxa_bootstrap_mqc.tsv
      description: Bootstrap of taxa
    - name: taxa_rds
      type: rds
      file: taxa.rds
      description: Taxa
    - name: otu_table
      type: csv
      file: otu_table.csv
      description: OTU table
    - name: tax_table_csv
      type: csv
      file: tax_table.csv
      description: Taxa table
    - name: richness
      type: png
      file: Richness_mqc.png
      description: Richness plot
    - name: top20
      type: png
      file: Top_20_mqc.png
      description: Top 20 OTU
    - name: ps_out
      type: rds
      file: ps_out.rds
      description: Phyloseq object
    - name: ordination
      type: png
      file: Ordination_mqc.png
      description: Ordination plot
    - name: tax_table_biom
      type: txt
      file: tax_table.qiime.txt
      description: Taxa table for qiime
    - name: out_table_biom
      type: biom
      file: otu_table.qiime.biom
      description: OTU table for qiime
multiqc:
  cutadapt: cutadapt
  'null': custom
  dada2: custom
stop_cases: {}
