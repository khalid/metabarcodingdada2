suppressMessages(library(dada2,quietly=T));print(paste0("dada2: ",packageVersion("dada2")))
suppressMessages(library(RcppParallel,quietly=T))

# Put a fixed seed to have the same random choice when learning errors (reproducibility)
set.seed(1)

######################
# Inspect read quality profiles
######################
quality <- function(){
    suppressMessages(library(ggplot2,quietly=T)); print(paste0("ggplot2: ",packageVersion("ggplot2")))
    png(filename = qualityForward, height=800, width=800)
    print(plotQualityProfile(fnFs[1:2]) + ggtitle("Quality profile forward reads"))
    dev.off()
    png(filename = qualityReverse, height=800, width=800)
    print(plotQualityProfile(fnRs[1:2]) + ggtitle("Quality profile reverse reads"))
    dev.off()
}

######################
# Filter and trim
######################

filter_and_trim <- function(){
    suppressMessages(library(ShortRead))
    out = data.frame()
    
    fnFs = sort(fnFs)
    sample.names = sort(sample.names)

    if(no_filter_and_trim & cutadapt_stats == ""){
        # count reads
        nbreads <- function (fl, chunksize){
            totseq = 0
            f <- FastqStreamer(fl, chunksize)
            while (length(fq <- yield(f))) {
                totseq = totseq + length(fq)
            }
            close(f)
            return(totseq)
        }
        for (i in 1:length(fnFs)){
            totseq = nbreads(fnFs[i],500)
            out = rbind(out,totseq)
        }
        rownames(out) = sample.names
    }
    if(no_filter_and_trim & cutadapt_stats != ""){
        stats = read.table(cutadapt_stats,header=T,sep="\t",row.names=1,stringsAsFactors=FALSE)
        out = stats
        colnames(out) = c("raw_reads","trimmed_reads")
    }
    if(!no_filter_and_trim){
        out <- filterAndTrim(fwd=fnFs, filt=filtFs, rev=fnRs, filt.rev=filtRs, truncLen=c(truncLen_forward, truncLen_reverse),
                maxN=0, maxEE=c(maxEE_forward, maxEE_reverse), truncQ=2, rm.phix=TRUE,
                compress=TRUE, multithread=threads, matchIDs=TRUE)
        rownames(out) = sample.names
        if(cutadapt_stats != ""){
            stats = read.table(cutadapt_stats,header=T,sep="\t",row.names=1,stringsAsFactors=FALSE)
            out = cbind(stats$raw,out)
            colnames(out) = c("raw_reads","trimmed_reads","dada2_filtered_reads")
            rownames(out) = rownames(stats)
        }
    }
    saveRDS(out,filter_and_trim_out)
}

######################
# Learn the Error Rates
######################
learn_errors <- function(){
    suppressMessages(library(ggplot2,quietly=T)); print(paste0("ggplot2: ",packageVersion("ggplot2")))
    errF <- learnErrors(filtFs,nbases = 1e8, multithread=TRUE,randomize=TRUE)
    errR <- learnErrors(filtRs,nbases = 1e8, multithread=TRUE,randomize=TRUE)

    png(filename = errorsForward, height=800, width=800)
    print(plotErrors(errF, nominalQ=TRUE) + ggtitle("Error profile forward reads"))
    dev.off()
    png(filename = errorsReverse, height=800, width=800)
    print(plotErrors(errR, nominalQ=TRUE) + ggtitle("Error profile reverse reads"))
    dev.off()
    saveRDS(errF,errF_rds)
    saveRDS(errR,errR_rds)
}

######################
# Sample Inference
# Merge paired reads
# Construct sequence table
######################

construct_seqtab <- function(){
    errF = readRDS(errF_rds)
    errR = readRDS(errR_rds)
    filter_and_trim_out = readRDS(filter_and_trim_out)
    getN <- function(x) sum(getUniques(x))

    mergers <- vector("list", length(sample.names))
    countFs <- vector("list", length(sample.names))
    countRs <- vector("list", length(sample.names))
    countMergers <- vector("list", length(sample.names))
    names(mergers) <- sample.names

    # sort to iterate the 3 lists simulteanously
    filtFs = sort(filtFs)
    filtRs = sort(filtRs)
    sample.names = sort(sample.names)
    
    for(i in 1:length(sample.names)) {
        sam = sample.names[i]
        Fsfile = filtFs[i]
        Rsfile = filtRs[i]
        
        cat("Processing:", sam, " : ",Fsfile," ,",Rsfile,"\n")
        ddF <- dada(Fsfile, err=errF, multithread=TRUE)
        ddR <- dada(Rsfile, err=errR, multithread=TRUE)
        merger <- mergePairs(ddF, Fsfile, ddR, Rsfile)

        countFs[[sam]] = getN(ddF)
        countRs[[sam]] = getN(ddR)
        countMergers[[sam]] = getN(merger)

        mergers[[sam]] <- merger
    }
    # Construct sequence table
    seqtab_obj <- makeSequenceTable(mergers)

    #Ne retenir que les sequences dont la taille est supérieur ou égale à 50 nt
    seqtab_obj <- seqtab_obj[,nchar(colnames(seqtab_obj)) >= 50 ]

    track_tab <- cbind(filter_and_trim_out[sample.names,], unlist(countFs[sample.names]), unlist(countRs[sample.names]), unlist(countMergers[sample.names]), rowSums(seqtab_obj[sample.names,]))
    colnames(track_tab) <- c(colnames(filter_and_trim_out), "forward", "reverse", "merged", "sup50")
    rownames(track_tab) <- sample.names

    cat("
# id: Track_reads
# section_name: 'Track reads'
# description: 'Monitoring the number of reads during the steps'
# format: 'tsv'
# plot_type: 'table'\n\"\"\t", file=track)

    write.table(track_tab,track,append=TRUE,sep="\t")

    uniquesToFasta(seqtab_obj, uniques)
    saveRDS(seqtab_obj, seqtab)
}

######################
# Remove chimeras
######################
remove_chimeras <- function(){
    seqtab_obj = readRDS(seqtab)
    seqtab.nochim <- removeBimeraDenovo(seqtab_obj, method="consensus", multithread=TRUE, verbose=TRUE)
    rm(seqtab_obj)
    gc() #garbage collector
    seqcounts = seqtab.nochim
    colnames(seqcounts) = paste0("seq",1:dim(seqcounts)[2])
    write.csv(seqcounts,seqtab_nochim_csv)
    rm(seqcounts)
    gc()
    uniquesToFasta(seqtab.nochim, uniques_nochim)
    saveRDS(seqtab.nochim,seqtab_nochim_rds)
}

######################
# Assign taxonomy
######################
assign_taxonomy <- function(){
    Taxonomypath <- taxonomypath
    seqtab_obj = readRDS(seqtab)
    taxa <- assignTaxonomy(seqtab_obj, paste0(Taxonomypath, "/train_set.fa.gz"), multithread=TRUE, outputBootstraps=TRUE)
    bootstrap = taxa$boot
    rownames(bootstrap) = paste0("seq",1:nrow(bootstrap))

    cat("
# id: Taxa_bootstrap
# section_name: 'Taxa bootstrap'
# description: 'Percentages of sequence assignment to the different taxonomic ranks.'
# format: 'tsv'
# plot_type: 'table'\n\"\"\t", file=taxa_bootstrap)

    write.table(bootstrap,taxa_bootstrap,append=TRUE,sep="\t")

    if (file.exists(paste0(Taxonomypath,"/assignment.fa.gz"))){
        taxa <- addSpecies(taxa$tax, paste0(Taxonomypath,"/assignment.fa.gz"))
        saveRDS(taxa,taxa_rds)
    }
    else{
        saveRDS(taxa$tax,taxa_rds)
    }
    
}

######################
# Phyloseq
######################

phyloseq_run <- function(){
    suppressMessages(library(phyloseq,quietly=T));print(paste0("phyloseq: ",packageVersion("phyloseq")))
    suppressMessages(library(ggplot2,quietly=T)); print(paste0("ggplot2: ",packageVersion("ggplot2")))
    seqtab_obj = readRDS(seqtab)
    taxa = readRDS(taxa_rds)
    # metadata = read.table(metadata,sep="\t")
    # colnames(metadata)[2] = "facteur"
    # rownames(metadata) = metadata[,1]

    # samples.out <- rownames(seqtab.nochim)

    #metadata = metadata[metadata[,1] %in% samples.out,]

    ps <- phyloseq(otu_table(seqtab_obj, taxa_are_rows=FALSE), 
    #               sample_data(metadata), 
                tax_table(taxa))

    rm(seqtab_obj)
    gc()

    write.csv(ps@otu_table,otu_table)
    write.csv(ps@tax_table,tax_table_csv)

    # GF = genefilter_sample(ps, filterfun_sample(function(x) x > 3), A=0.1*nsamples(ps))
    # ps = prune_taxa(GF, ps)

    png(filename = richness, height=800, width=800)
    #plot_richness(ps, x="facteur", measures=c("Shannon", "Simpson"))
    print(plot_richness(ps, measures=c("Shannon", "Simpson"),title="Alpha-diversity per sample"))
    dev.off()

    tryCatch({
        #ps.prop <- transform_sample_counts(ps, function(otu) otu/sum(otu))

        # TO TEST AGAIN species.sum = tapply(taxa_sums(ps), tax_table(ps)[, "Species"], sum, na.rm=TRUE)
        # TO TEST AGAIN top_species = names(phylum.sum)
        # TO TEST AGAIN ps2 = prune_taxa((tax_table(ps)[, "Species"] %in% top_species), ps)

        ord.nmds.bray <- ordinate(ps, method="NMDS", distance="bray")
        png(filename = ordination, height=800, width=800)
        print(plot_ordination(ps, ord.nmds.bray,title="Bray NMDS"))
        dev.off()
    }, error = function(e) {
        png(filename = ordination, height=800, width=800)
        #par(mar =c(0,0,0,0))
        print(plot(c(0,1),c(0,1),ann =F,bty ='n',type ='n',xaxt ='n',yaxt ='n'))
        text(x =0.5,y =0.5,paste("Problem with phyloseq ordination function"),cex =1.6,col ="black")
        dev.off()
    })
    
    top20_obj <- names(sort(taxa_sums(ps), decreasing=TRUE))[1:20]
    ps.top20 <- transform_sample_counts(ps, function(OTU) OTU/sum(OTU))
    ps.top20 <- prune_taxa(top20_obj, ps.top20)
    
    tryCatch({
        png(filename = top20, height=800, width=800)
        print(plot_bar(ps.top20, fill="Family", title="Top 20"))#, x="facteur") + facet_wrap(~When, scales="free_x")
        dev.off()
    }, error = function(e) {
        png(filename = top20, height=800, width=800)
        #par(mar =c(0,0,0,0))
        print(plot(c(0,1),c(0,1),ann =F,bty ='n',type ='n',xaxt ='n',yaxt ='n'))
        text(x =0.5,y =0.5,paste("Problem with phyloseq, no families to plot"),cex =1.6,col ="black")
        dev.off()
    })

    saveRDS(ps,ps_out)
}

export_qiime <- function(){
    suppressMessages(library(phyloseq,quietly=T));print(paste0("phyloseq: ",packageVersion("phyloseq")))
    suppressMessages(library(biomformat,quietly=T));print(paste0("biomformat: ",packageVersion("biomformat")))
    # Export for QIIME
    # Export taxonomy table as "tax.txt"
    ps = readRDS(ps_out)
    tax<-as(tax_table(ps),"matrix")
    tax_cols <- colnames(tax)
    tax<-as.data.frame(tax)
    tax$taxonomy<-do.call(paste, c(tax[tax_cols], sep=";"))
    for(co in tax_cols) tax[co]<-NULL
    write.table(tax, tax_table_biom, quote=FALSE, col.names=FALSE, sep="\t")

    rm(tax)
    gc() #garbage collector
    # Export feature/OTU table

    # As a biom file

    otu<-t(as(otu_table(ps),"matrix")) # 't' to transform if taxa_are_rows=FALSE
    #if taxa_are_rows=TRUE
    #otu<-as(otu_table(GlobalPatterns),"matrix"))
    otu_biom<-make_biom(data=otu)
    write_biom(otu_biom, otu_table_biom)
}

plot_krona <- function(){
    suppressMessages(library(phyloseq,quietly=T));print(paste0("phyloseq: ",packageVersion("phyloseq")))
   physeq = readRDS(ps_rds)
   # Melt the OTU table and merge associated metadata
   df<-psmelt(physeq)
   # Fetch only Abundance, Description and taxonomic rank names columns
   df<-df[ ,c("Sample","Abundance", intersect(rank_names(physeq),colnames(df))) ]

   # Create a directory for krona files
   dir.create(output_dir)

   # For each Sample of the phyloseq obj
   # Abundance and taxonomic assignations for each OTU are fetched
   # and written to a file that would be processed by Krona.
   for( samp in unique(df$Sample) ){
     write.table(
         df[ which( df$Sample == samp), -1],
       file = paste0(output_dir,"/",samp, "taxonomy.txt"),
       sep = "\t",row.names = F,col.names = F,na = "",quote = F)
   }
   # Arguments for Krona command
   # taxonomic file and their associated labels.
   krona_args<-paste(output_dir,"/",unique(df$Sample),
                     "taxonomy.txt,",
                     unique(df$Sample),
                     sep = "", collapse = " ")
   # Add html suffix to output
   output<-paste(output_dir,"/text.krona.html",sep = "")
   # Execute Krona command
   system(paste("ktImportText",
                krona_args,
                "-o", output,
                sep = " "))
    cat(paste0("1\t./dada2/Krona_files/text.krona.html"),file=paste0(output_dir,"/text.krona.tsv"))
}

# Change to TRUE to use without snakemake
standalone_mode = FALSE
if (standalone_mode == FALSE) {
    step = snakemake@params[["step"]]
    threads = snakemake@threads

    if ("cutadapt_stats" %in% names(snakemake@input)){
        cutadapt_stats = snakemake@input[["cutadapt_stats"]]
    } else {
        cutadapt_stats = ""
    }

    errF_rds = snakemake@input[["errF_rds"]]
    errR_rds = snakemake@input[["errR_rds"]]

    if (is.null(errF_rds)){
        errF_rds = snakemake@output[["errF_rds"]]
        errR_rds = snakemake@output[["errR_rds"]]
    }

    filter_and_trim_out = snakemake@input[["filter_and_trim_out"]]
    if (is.null(filter_and_trim_out)){
        filter_and_trim_out = snakemake@output[["filter_and_trim_out"]]
    }
    
    filtFs = snakemake@input[["filtFs"]]
    filtRs = snakemake@input[["filtRs"]]
    if (is.null(filtFs)){
        filtFs = snakemake@output[["filtFs"]]
        filtRs = snakemake@output[["filtRs"]]
    }

    metadata = snakemake@input[["metadata"]]
    
    ps_out = snakemake@input[["ps_out"]]
    if (is.null(ps_out)){
        ps_out = snakemake@output[["ps_out"]]
    }

    read = snakemake@input[["read"]]
    read2 = snakemake@input[["read2"]]

    seqtab = snakemake@input[["seqtab"]]
    if (is.null(seqtab)){
        seqtab = snakemake@output[["seqtab"]]
    }

    taxa_rds = snakemake@input[["taxa_rds"]]
    if (is.null(taxa_rds)){
        taxa_rds = snakemake@output[["taxa_rds"]]
    }
    
    ps_rds = snakemake@input[["ps_rds"]]

    errorsForward = snakemake@output[["errorsForward"]]
    errorsReverse = snakemake@output[["errorsReverse"]]
    
    ordination = snakemake@output[["ordination"]]
    otu_table = snakemake@output[["otu_table"]]
    otu_table_biom = snakemake@output[["otu_table_biom"]]

    qualityForward = snakemake@output[["qualityForward"]]
    qualityReverse = snakemake@output[["qualityReverse"]]
    richness = snakemake@output[["richness"]]
    
    seqtab_nochim_csv = snakemake@output[["seqtab_nochim_csv"]]
    seqtab_nochim_rds = snakemake@output[["seqtab_nochim_rds"]]
    taxa_bootstrap = snakemake@output[["taxa_bootstrap"]]
    
    tax_table_biom = snakemake@output[["tax_table_biom"]]
    tax_table_csv = snakemake@output[["tax_table_csv"]]
    top20 = snakemake@output[["top20"]]
    track = snakemake@output[["track"]]
    uniques = snakemake@output[["uniques"]]
    uniques_nochim = snakemake@output[["uniques_nochim"]]

    datapath = snakemake@params[["datapath"]]
    maxEE_forward = snakemake@params[["maxEE_forward"]]
    maxEE_reverse = snakemake@params[["maxEE_reverse"]]
    output_dir = snakemake@params[["output_dir"]]
    samples = snakemake@params[["samples"]]
    taxonomypath = snakemake@params[["taxonomypath"]]
    truncLen_forward = snakemake@params[["truncLen_forward"]]
    truncLen_reverse = snakemake@params[["truncLen_reverse"]]
    no_filter_and_trim = snakemake@params[["no_filter_and_trim"]]

} else {
    step = "all"
    threads = 4
    errF_rds = ""
    errR_rds = ""
    filter_and_trim_out = ""
    filtFs = ""
    filtRs = ""
    metadata = ""
    ps_out = ""
    read = ""
    read2 = ""
    seqtab = ""
    taxa_rds = ""
    errorsForward = ""
    errorsReverse = ""
    ordination = ""
    otu_table = ""
    otu_table_biom = ""
    qualityForward = ""
    qualityReverse = ""
    richness = ""
    seqtab_nochim_csv = ""
    seqtab_nochim_rds = ""
    taxa_bootstrap = ""
    tax_table_biom = ""
    tax_table_csv = ""
    top20 = ""
    track = ""
    uniques = ""
    uniques_nochim = ""
    datapath = ""
    maxEE_forward = ""
    maxEE_reverse = ""
    output_dir = ""
    samples = ""
    taxonomypath = ""
    truncLen_forward = ""
    truncLen_reverse = ""
    cutadapt_stats = ""
    no_filter_and_trim = FALSE
    ps_rds = ""
}

Datapath <- datapath
fnFs <- read
fnRs <- read2
sample.names <- samples

setThreadOptions(numThreads = threads)

if (step == "quality" | step == "all"){
    quality()
}
if (step == "filter_and_trim" | step == "all"){
    filter_and_trim()
}
if (step == "learn_errors" | step == "all"){
    learn_errors()
}
if (step == "construct_seqtab" | step == "all"){
    construct_seqtab()
}
if (step == "remove_chimeras" | step == "all"){
    remove_chimeras()
}
if (step == "assign_taxonomy" | step == "all"){
    assign_taxonomy()
}
if (step == "phyloseq_run" | step == "all"){
    phyloseq_run()
}
if (step == "export_qiime" | step == "all"){
    export_qiime()
}
if (step == "krona" | step == "all"){
    plot_krona()
}